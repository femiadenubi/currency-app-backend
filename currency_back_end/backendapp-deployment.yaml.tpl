apiVersion: apps/v1
kind: Deployment
metadata:
  name: currency_back_end
  labels:
    app: currency_back_end
spec:
  replicas: 1
  selector:
    matchLabels:
      app: currency_back_end
  template:
    metadata:
      labels:
        app: currency_back_end
    spec:
      volumes:
        - name: firebase-secret-key
          secret:
            secretName: firebase-auth-secret-key
      containers:
        - name: currency_back_end
          image: gcr.io/GOOGLE_CLOUD_PROJECT/IMAGE_NAME:COMMIT_SHA
          ports:
            - containerPort: 8080
          volumeMounts:
            - name: firebase-secret-key
              mountPath: /var/secrets/firebase
              readOnly: true
          envFrom:
            - configMapRef:
                name: currency_back_end-config
            - secretRef:
                name: database-credentials
          env:
            - name: GOOGLE_APPLICATION_CREDENTIALS
              value: /var/secrets/firebase/key.json
            - name: SPRING_PROFILES_ACTIVE
              value: prod
---
apiVersion: v1
kind: Service
metadata:
  name: currency_back_end
spec:
  selector:
    app: currency_back_end
  ports:
    - protocol: TCP
      port: 80
      targetPort: 8080
  type: ClusterIP